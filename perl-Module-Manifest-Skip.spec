Name:           perl-Module-Manifest-Skip
Version:        0.23
Release:        18
Summary:        MANIFEST.SKIP Manangement for Modules
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Module-Manifest-Skip

Source0:        https://cpan.metacpan.org/authors/id/I/IN/INGY/Module-Manifest-Skip-%{version}.tar.gz
Patch0:         Module-Manifest-Skip-0.23-Adapt-to-changes-in-Moo-2.004000.patch

BuildArch:      noarch
BuildRequires:  perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.30 
BuildRequires:  perl(File::ShareDir::Install) >= 0.06 perl(strict) perl(warnings)
BuildRequires:  perl(File::ShareDir) perl(File::Spec) perl(Moo) >= 0.091013
BuildRequires:  perl(base) perl(Cwd) perl(Exporter) perl(lib) perl(Test::More)
Requires:       perl(File::ShareDir) perl(File::Spec) perl(Moo) >= 0.091013 perl(warnings)

%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Moo\\)$

%description
CPAN module authors use a "MANIFEST.SKIP" file to exclude certain well known files from getting put
into a generated "MANIFEST" file, which would cause them to go into the final distribution package.
 
The packaging tools try to automatically skip things for you, but if you add one of your own entries,
you have to add all the common ones yourself. This module attempts to make all of this boring process 
as simple and reliable as possible.
 
Module::Manifest::Skip can create or update a MANIFEST.SKIP file for you. You can add your own entries, 
and it will leave them alone. You can even tell it to not skip certain entries that it normally skips,
although this is rarely needed.

%package_help

%prep
%autosetup -n Module-Manifest-Skip-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc README
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc Changes CONTRIBUTING
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.23-18
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jun 15 2022 yaoxin <yaoxin30@h-partners.com> - 0.23-17
- Adapt to changes in perl-Moo update to 2.005004

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.23-16
- Package init
